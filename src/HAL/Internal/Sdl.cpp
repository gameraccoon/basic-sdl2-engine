#include "Sdl.h"

#include "Debug/Assert.h"

#include <sdl/SDL.h>
#include <stdexcept>

namespace HAL
{
	namespace Internal
	{
		SDLInstance::SDLInstance(unsigned int flags)
		{
			if (SDL_Init(flags) != 0)
			{
				ReportFatalError("Failed to init SDL");
			}
		}

		SDLInstance::~SDLInstance()
		{
			SDL_Quit();
		}
	}
}
