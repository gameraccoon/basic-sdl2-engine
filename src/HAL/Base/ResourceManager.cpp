#include "HAL/Base/ResourceManager.h"

#include <fstream>
#include <vector>
#include <experimental/filesystem>

#include "Debug/Log.h"
#include "Debug/Assert.h"

#include "HAL/Base/Engine.h"
#include "HAL/Internal/SdlSurface.h"

namespace HAL
{
	static Graphics::Texture EMPTY_TEXTURE = Graphics::Texture();
	static Graphics::Font EMPTY_FONT = Graphics::Font();
	static Audio::Sound EMPTY_SOUND = Audio::Sound();
	static Audio::Music EMPTY_MUSIC = Audio::Music();

	ResourceManager::ResourceManager(Engine* engine)
		: mEngine(engine)
	{
	}

	template<>
	const Graphics::Texture& ResourceManager::getEmptyResource<Graphics::Texture>()
	{
		return EMPTY_TEXTURE;
	}

	template<>
	const Graphics::Font& ResourceManager::getEmptyResource<Graphics::Font>()
	{
		return EMPTY_FONT;
	}

	template<>
	const Audio::Sound& ResourceManager::getEmptyResource<Audio::Sound>()
	{
		return EMPTY_SOUND;
	}

	template<>
	const Audio::Music& ResourceManager::getEmptyResource<Audio::Music>()
	{
		return EMPTY_MUSIC;
	}

	void ResourceManager::createResourceLock(const std::string& path)
	{
		mPathsMap[path] = mHandleIdx;
		mPathFindMap[mHandleIdx] = path;
		mResourceLocksCount[mHandleIdx] = 1;
	}

	ResourceHandle ResourceManager::lockTexture(const std::string& path)
	{
		auto it = mPathsMap.find(path);
		if (it != mPathsMap.end())
		{
			++mResourceLocksCount[it->second];
			return ResourceHandle(it->second);
		}
		else
		{
			createResourceLock(path);
			mResources[mHandleIdx] = std::make_unique<Graphics::Texture>(path, mEngine->getRenderer()->getRawRenderer());
			return ResourceHandle(mHandleIdx++);
		}
	}

	ResourceHandle ResourceManager::lockFont(const std::string& path, int fontSize)
	{
		std::string id = path + ":" + std::to_string(fontSize);
		auto it = mPathsMap.find(id);
		if (it != mPathsMap.end())
		{
			++mResourceLocksCount[it->second];
			return ResourceHandle(it->second);
		}
		else
		{
			createResourceLock(id);
			mResources[mHandleIdx] = std::make_unique<Graphics::Font>(path, fontSize, mEngine->getRenderer()->getRawRenderer());
			return ResourceHandle(mHandleIdx++);
		}
	}

	ResourceHandle ResourceManager::lockSound(const std::string& path)
	{
		auto it = mPathsMap.find(path);
		if (it != mPathsMap.end())
		{
			++mResourceLocksCount[it->second];
			return ResourceHandle(it->second);
		}
		else
		{
			createResourceLock(path);
			mResources[mHandleIdx] = std::make_unique<Audio::Sound>(path);
			return ResourceHandle(mHandleIdx++);
		}
	}

	ResourceHandle ResourceManager::lockMusic(const std::string& path)
	{
		auto it = mPathsMap.find(path);
		if (it != mPathsMap.end())
		{
			++mResourceLocksCount[it->second];
			return ResourceHandle(it->second);
		}
		else
		{
			createResourceLock(path);
			mResources[mHandleIdx] = std::make_unique<Audio::Music>(path);
			return ResourceHandle(mHandleIdx++);
		}
	}

	void ResourceManager::unlockResource(ResourceHandle handle)
	{
		auto locksCntIt = mResourceLocksCount.find(handle.ResourceIndex);
		AssertRetVoid(locksCntIt != mResourceLocksCount.end(), "Unlocking non-locked resource");
		if (locksCntIt->second > 1)
		{
			--(locksCntIt->second);
			return;
		}
		else
		{
			// unload resource
			auto resourceIt = mResources.find(handle.ResourceIndex);
			if (resourceIt != mResources.end())
			{
				auto releaseFnIt = mResourceReleaseFns.find(handle.ResourceIndex);
				if (releaseFnIt != mResourceReleaseFns.end())
				{
					releaseFnIt->second(resourceIt->second.get());
					mResourceReleaseFns.erase(releaseFnIt);
				}
				mResources.erase(resourceIt);
			}
			mResourceLocksCount.erase(handle.ResourceIndex);
			auto pathIt = mPathFindMap.find(handle.ResourceIndex);
			if (pathIt != mPathFindMap.end())
			{
				mPathsMap.erase(pathIt->second);
			}
			mPathFindMap.erase(handle.ResourceIndex);
		}
	}
}
