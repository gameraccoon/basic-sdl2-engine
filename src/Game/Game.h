#pragma once

#include "Utils/ResourceHandle.h"
#include "Utils/Math/Vector2D.h"

#include "HAL/GameBase.h"
#include "HAL/KeyStatesMap.h"

class Game : public HAL::GameBase
{
public:
	using GameBase::GameBase;

	void start();
	void update(float dt) override;
	void initResources() override;
	void setKeyboardKeyState(int key, bool isPressed) override;
	void setMouseKeyState(int key, bool isPressed) override;

private:
	void testUpdate(float dt);

private:
	HAL::KeyStatesMap mKeyboardKeyStates;
	HAL::KeyStatesMap mMouseKeyStates;

	ResourceHandle mTestTexture;
	ResourceHandle mTestFont;
	ResourceHandle mMusic;
	ResourceHandle mPingSound;
	ResourceHandle mPongSound;
	Vector2D mTestPos = ZERO_VECTOR;
	Vector2D mTestSpeed = ZERO_VECTOR;
};
