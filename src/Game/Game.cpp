#include "Game/Game.h"

#include "Debug/Log.h"

#include <sdl/SDL_keycode.h>

#include "Utils/Math/Vector2D.h"

#include "HAL/Base/Engine.h"
#include "HAL/Base/ResourceManager.h"
#include "HAL/Audio/AudioManager.h"

void Game::start()
{
	LogInit("Game started");

	// give some initial values
	mTestPos = Vector2D(
		static_cast<float>(rand() % getEngine()->getWidth()),
		static_cast<float>(rand() % getEngine()->getHeight()));
	mTestSpeed = Vector2D(static_cast<float>(rand() % 300), static_cast<float>(rand() % 300));

	// start the main loop
	getEngine()->start(this);
}

void Game::setKeyboardKeyState(int key, bool isPressed)
{
	mKeyboardKeyStates.updateState(key, isPressed);
}

void Game::setMouseKeyState(int key, bool isPressed)
{
	mMouseKeyStates.updateState(key, isPressed);
}

void Game::update(float dt)
{
	testUpdate(dt);
}

void Game::initResources()
{
	// resources initialization
	mTestTexture = getResourceManager()->lockTexture("resources/textures/testCircle.png");
	mTestFont = getResourceManager()->lockFont("resources/fonts/prstart.ttf", 16);

	mMusic = getResourceManager()->lockMusic("resources/sound/music/beat.wav");
	mPingSound = getResourceManager()->lockSound("resources/sound/effects/ping.wav");
	mPongSound = getResourceManager()->lockSound("resources/sound/effects/pong.wav");

	Audio::AudioManager::SetMusicVolume(0.1f);
	const Audio::Music& music = getResourceManager()->getResource<Audio::Music>(mMusic);
	if (music.isValid())
	{
		Audio::AudioManager::PlayMusic(music);
	}
}

void Game::testUpdate(float dt)
{
	// process input
	Vector2D movementDirection(0.0f, 0.0f);
	if (mKeyboardKeyStates.isPressed(SDLK_LEFT))
	{
		movementDirection += LEFT_DIRECTION;
	}

	if (mKeyboardKeyStates.isPressed(SDLK_RIGHT))
	{
		movementDirection += RIGHT_DIRECTION;
	}

	if (mKeyboardKeyStates.isPressed(SDLK_UP))
	{
		movementDirection += UP_DIRECTION;
	}

	if (mKeyboardKeyStates.isPressed(SDLK_DOWN))
	{
		movementDirection += DOWN_DIRECTION;
	}

	// process movement
	constexpr float acceleration = 100.0f;
	mTestSpeed += movementDirection * acceleration * dt;
	mTestPos += mTestSpeed * dt;

	// process borders collision
	static const Vector2D size = { 30.0f, 30.0f };
	bool hasCollision = false;
	if (mTestPos.x < size.x*0.5f && mTestSpeed.x < 0.0f)
	{
		mTestSpeed.x = -mTestSpeed.x;
		hasCollision = true;
	}
	if (mTestPos.x > getEngine()->getWidth() - size.x*0.5f && mTestSpeed.x > 0.0f)
	{
		mTestSpeed.x = -mTestSpeed.x;
		hasCollision = true;
	}
	if (mTestPos.y < size.y*0.5f && mTestSpeed.y < 0.0f)
	{
		mTestSpeed.y = -mTestSpeed.y;
		hasCollision = true;
	}
	if (mTestPos.y > getEngine()->getHeight() - size.y*0.5f && mTestSpeed.y > 0.0f)
	{
		mTestSpeed.y = -mTestSpeed.y;
		hasCollision = true;
	}

	// process render
	Graphics::Renderer* renderer = getEngine()->getRenderer();
	const Graphics::Texture& testTexture = getResourceManager()->getResource<Graphics::Texture>(mTestTexture);
	if (testTexture.isValid())
	{
		renderer->render(testTexture, mTestPos, size, Vector2D(0.5f, 0.5f), 0.0f);
	}

	const Graphics::Font& testFont = getResourceManager()->getResource<Graphics::Font>(mTestFont);
	if (testFont.isValid())
	{
		renderer->renderText(testFont, Vector2D(100.f, 100.f), { 0, 255, 0, 255 }, "This is example.\nIt works.");
	}

	// process sounds
	if (hasCollision)
	{
		const Audio::Sound& hitSound = getResourceManager()->getResource<Audio::Sound>((rand() % 2 == 0) ? mPingSound : mPongSound);
		if (hitSound.isValid())
		{
			Audio::AudioManager::PlaySound(hitSound);
		}
	}
}
