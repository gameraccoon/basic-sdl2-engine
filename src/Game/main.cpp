#include "Game/Game.h"

#include "Utils/Application/ArgumentsParser.h"
#include <time.h>

int main(int argc, char** argv)
{
	ArgumentsParser arguments(argc, argv);

	int windowWidth = std::stoi(arguments.getArgumentValue("windowWidth", "800"));
	int windowHeight = std::stoi(arguments.getArgumentValue("windowHeight", "600"));

	int seed = std::stoi(arguments.getArgumentValue("randomSeed", std::to_string(time(0))));
	srand(seed);

	Game game(windowWidth, windowHeight);
	game.start();

	return 0;
}
